# Ruby Virtual Pet Notebook

## 1. Introduction to Ruby

Ruby is a dynamic, object-oriented programming language known for its simplicity and productivity.
This is a Ruby notebook focused on exploring language concepts through the use of virtual pets as illustrative examples.

## 2. Basics of Ruby

### 2.1 Variables

- **Declaration**: Variables are declared using a simple assignment, e.g., `pet_name = "Bobby"`.
- **Naming Conventions**: Use snake_case for variable names (`my_variable`) and start with a lowercase letter.
- **Scope**: Variable scope depends on where it's defined - local, instance, class, or global.

**Example:**

```ruby
pet_name = "Bobby"
```

**Explanation:**
- **Declaration**: The variable `pet_name` is declared and assigned the value "Bobby."
- **Naming Conventions**: The variable name `pet_name` follows the snake_case convention, starting with a lowercase letter.
- **Clean Code**: The variable name is descriptive, conveying that it represents the name of the virtual pet.

### 2.2 Data Types

#### 2.2.1 Numeric Types

- **Integers (`Fixnum` and `Bignum`)**: Whole numbers. Operations like addition, subtraction, multiplication, and division apply.
- **Floats**: Numbers with decimal points. Floating-point arithmetic is used.

**Example:**

```ruby
hunger_level = 80
happiness_level = 60.5
```

**Explanation:**
- **Numeric Types**: Two variables, `hunger_level` and `happiness_level`, are assigned numeric values.
- **Clean Code**: The variable names are clear and meaningful, indicating the purpose of each variable.

#### 2.2.2 Strings

- **Definition**: Enclosed in single (`'`) or double (`"`) quotes.
- **Concatenation**: Use `+` to concatenate strings.
- **Interpolation**: Embed variables in strings using `#{variable}`.
- **Methods**: Strings have various methods for manipulation, e.g., `length`, `upcase`, `downcase`.

**Example:**

```ruby
favorite_toy = "Ball"
```

**Explanation:**
- **String**: The variable `favorite_toy` is assigned the string value "Ball."
- **Clean Code**: The variable name is expressive, revealing that it represents the pet's favorite toy.

#### 2.2.3 Symbols

- **Definition**: Immutable identifiers, often used as keys in hashes.
- **Colon Notation**: Symbols are written as `:symbol_name`.

**Example:**

```ruby
pet_type = :cat
```

**Explanation:**
- **Symbols**: The variable `pet_type` is assigned the symbol `:cat`.
- **Clean Code**: The use of a symbol is appropriate when representing a fixed, immutable identifier.

#### 2.2.4 Arrays

- **Definition**: Ordered lists of items.
- **Accessing Elements**: Done by index (starting from 0), e.g., `my_array[0]`.
- **Common Operations**: `push`, `pop`, `shift`, `unshift`, `join`, and more.

**Example:**

```ruby
pet_tricks = ["Roll Over", "Sit", "High-Five"]
```

**Explanation:**
- **Arrays**: The variable `pet_tricks` is an array storing the pet's tricks.
- **Clean Code**: The array is appropriately named, and its purpose is evident.

#### 2.2.5 Hashes

- **Definition**: Collections of key-value pairs.
- **Accessing Values**: Use keys to retrieve values, e.g., `my_hash[:key]`.
- **Common Operations**: `merge`, `keys`, `values`, `delete`, and more.

**Example:**

```ruby
pet_attributes = { :color => "Brown", :weight => 5.5, :breed => "Tabby" }
```

**Explanation:**
- **Hashes**: The variable `pet_attributes` is a hash storing various attributes of the pet.
- **Clean Code**: Symbol keys are used, enhancing readability and immutability.

### 2.3 Operations

#### 2.3.1 Arithmetic Operations

- **Addition**: `+`
- **Subtraction**: `-`
- **Multiplication**: `*`
- **Division**: `/`
- **Exponentiation**: `**`
- **Modulus**: `%`

**Example:**

```ruby
hunger_level -= 20
happiness_level += 10
```

**Explanation:**
- **Arithmetic Operations**: Adjustments are made to the `hunger_level` and `happiness_level` variables.
- **Clean Code**: Compound assignment operators (`-=`, `+=`) are used for concise and readable code.

#### 2.3.2 String Operations

- **Concatenation**: `+`
- **Repetition**: `*`

**Example:**

```ruby
pet_description = "Meet " + pet_name + ", your lovely pet."
```

**Explanation:**
- **String Concatenation**: The `pet_description` string is created by concatenating strings.
- **Clean Code**: While concatenation is used, a more idiomatic approach would be string interpolation.

#### 2.3.3 Comparison Operators

- **Equal**: `==`
- **Not Equal**: `!=`
- **Greater Than**: `>`
- **Less Than**: `<`
- **Greater Than or Equal To**: `>=`
- **Less Than or Equal To**: `<=`

**Example:**

```ruby
is_hungry = hunger_level > 70
is_happy = happiness_level >= 60
```

**Explanation:**
- **Comparison Operators**: Variables `is_hungry` and `is_happy` store boolean values based on comparisons.
- **Clean Code**: The variable names convey the purpose clearly, but consider more descriptive names for readability.

### 2.4 Manipulation and Accessing

- **String Manipulation**: Use methods like `sub`, `gsub`, `split`, and `slice`.
- **Array Manipulation**: Utilize methods such as `push`, `pop`, `shift`, `unshift`, and `slice`.
- **Hash Manipulation**: Employ methods like `merge`, `delete`, `each`, and `select`.
- **Variable Assignment**: Use `=` to assign a new value to a variable.
- **Destructive Operations**: Some methods have a destructive counterpart ending with `!`, e.g., `upcase!`, which modifies the original object.

**Example:**

```ruby
pet_info = "#{pet_name} is a #{pet_type} who loves playing with a #{favorite_toy}."
```

**Explanation:**
- **String Interpolation**: The `pet_info` string is created using interpolation, embedding variables within the string.
- **Clean Code**: Interpolation is a cleaner and more readable way to combine variables with strings.

```ruby
pet_tricks << "Fetch"
puts "#{pet_name} can do the trick: #{pet_tricks[1]}"
```

**Explanation:**
- **Array Manipulation**: The `pet_tricks` array is used to store and manipulate the pet's tricks.
- **Clean Code**: The array operations are clear, but consider using the shovel operator (`<<`) for

 adding elements.

```ruby
pet_attributes[:color] = "Gray"
```

**Explanation:**
- **Hash Manipulation**: The `pet_attributes` hash is used to store and manipulate the pet's attributes.
- **Clean Code**: The hash operations are straightforward, and the use of symbols as keys is appropriate for immutable identifiers.

## 3. Control Flow

### 3.1 Conditional Statements

#### 3.1.1 If Statement

- **Syntax**: Use `if`, `elsif`, and `else` for conditional branching.
- **Indentation**: Maintain consistent indentation for readability.

**Example:**

```ruby
if is_hungry
  puts "#{pet_name} is hungry. Time to feed!"
elsif is_happy
  puts "#{pet_name} is happy and content."
else
  puts "#{pet_name} has neutral feelings."
end
```

**Explanation:**
- **Conditional Statement**: The `if` statement checks the values of `is_hungry` and `is_happy` to determine the pet's state.
- **Clean Code**: Indentation enhances the readability of the code.

#### 3.1.2 Ternary Operator

- **Syntax**: Utilize the ternary operator (`condition ? true_value : false_value`) for concise conditional expressions.

**Example:**

```ruby
mood = is_happy ? "joyful" : "neutral"
```

**Explanation:**
- **Ternary Operator**: The variable `mood` is assigned a value based on the condition of `is_happy`.
- **Clean Code**: The ternary operator provides a compact and readable way to express simple conditions.

### 3.2 Loops

#### 3.2.1 While Loop

- **Syntax**: Use `while` for a basic loop structure.

**Example:**

```ruby
while hunger_level > 0
  puts "#{pet_name} is still hungry. Keep feeding!"
  hunger_level -= 10
end
```

**Explanation:**
- **While Loop**: The loop continues until the pet's hunger level reaches zero, with updates to `hunger_level`.
- **Clean Code**: The loop's purpose is clear, and the code is well-structured.

#### 3.2.2 Each Iterator

- **Syntax**: Employ `each` for iterating over elements in an array or hash.

**Example:**

```ruby
pet_tricks.each do |trick|
  puts "#{pet_name} can perform the trick: #{trick}"
end
```

**Explanation:**
- **Each Iterator**: The loop iterates through each element in `pet_tricks` and prints a message.
- **Clean Code**: The use of `each` enhances readability, and the block variable name (`trick`) is descriptive.

## 4. Methods

### 4.1 Definition and Invocation

- **Syntax**: Define methods using `def` and invoke them with `method_name()`.

**Example:**

```ruby
def introduce_pet
  puts "Meet #{pet_name}, your adorable #{pet_type}."
end
```

**Explanation:**
- **Method Definition and Invocation**: The `introduce_pet` method is defined and then invoked.
- **Clean Code**: Descriptive method names and clear invocation contribute to code readability.

### 4.2 Parameters

- **Passing Parameters**: Include parameters in method definition and pass arguments during invocation.

**Example:**

```ruby
def feed_pet(food_type)
  puts "#{pet_name} is enjoying #{food_type}."
  hunger_level -= 15
end
```

**Explanation:**
- **Parameter Usage**: The `feed_pet` method accepts a `food_type` parameter, and the method is invoked with the argument `"kibble"`.
- **Clean Code**: Using meaningful parameter names improves code clarity.

### 4.3 Return Statement

- **Return Values**: Utilize `return` to specify a method's return value.

**Example:**

```ruby
def calculate_happiness
  happiness_level = 100 - hunger_level
  return happiness_level
end

result = calculate_happiness
puts "Current happiness level: #{result}"
```

**Explanation:**
- **Return Statement**: The `calculate_happiness` method returns the calculated `happiness_level`.
- **Clean Code**: Explicitly using `return` enhances code readability.

## 5. Object-Oriented Programming (OOP)

### 5.1 Classes and Instances

- **Class Definition**: Define classes using `class` keyword.

**Example:**

```ruby
class VirtualPet
  def initialize(name, type)
    @name = name
    @type = type
  end

  def introduce
    puts "Meet #{@name}, your lovely #{@type}."
  end
end
```

**Explanation:**
- **Class and Instance**: The `VirtualPet` class is defined with an `initialize` method. An instance is then created and introduced.
- **Clean Code**: Clear class and method names contribute to code readability.
