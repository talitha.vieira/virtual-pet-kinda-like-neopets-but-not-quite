# Understanding Ruby through Exercism Challenges

## Hello World Exercise

```ruby
class HelloWorld
  def self.hello
    "Hello, World!"
  end
end
```

**Ruby Explanation:**

- **Class Declaration (`class HelloWorld`):** In Ruby, classes encapsulate behavior. The `class HelloWorld` syntax is used to define a class named `HelloWorld`.

- **Class Method (`def self.hello`):** The `def` keyword defines a method within the class. The `self.` before the method name denotes it as a class method. In Java terms, this is similar to a static method, called on the class itself rather than an instance.

- **Method Body (`"Hello, World!"`):** The method contains a single line of code, a string literal. In Ruby, the last evaluated expression is the return value, and here, the string is implicitly returned.

- **End of Method (`end`):** The `end` keyword marks the conclusion of the method definition, just as curly braces `{}` are used in Java.

- **End of Class (`end`):** The second `end` keyword closes the `HelloWorld` class, similar to the closing brace in Java. It indicates the end of the class definition.

**Usage:**

To use the `hello` method, call it directly on the class without creating an instance:

```ruby
puts HelloWorld.hello
```

This code would output "Hello, World!" to the console. In Java, a static method could be created in a class and called in a similar manner, highlighting the conceptual similarity between Ruby class methods and Java static methods.

```java
public class HelloWorld {

    public static String hello() {
        return "Hello, World!";
    }

    public static void main(String[] args) {
        System.out.println(HelloWorld.hello());
    }
}
```

## Lasagna Exercise

```ruby
class Lasagna
  EXPECTED_MINUTES_IN_OVEN = 40
    
  def remaining_minutes_in_oven(actual_minutes_in_oven)
    EXPECTED_MINUTES_IN_OVEN - actual_minutes_in_oven
  end
    
  def preparation_time_in_minutes(layers)
    layers * 2
  end
    
  def total_time_in_minutes(number_of_layers:, actual_minutes_in_oven:)
    actual_minutes_in_oven + preparation_time_in_minutes(number_of_layers)
  end
end
```

**Ruby Explanation:**

- **Class Declaration (`class Lasagna`):** The `class Lasagna` syntax is used to define a class named `Lasagna`. In Ruby, classes encapsulate behavior.

- **Constants (`EXPECTED_MINUTES_IN_OVEN = 40`):** A constant is defined using uppercase letters, representing the expected minutes for lasagna to be in the oven.

- **Instance Method (`def remaining_minutes_in_oven`):** This method calculates the remaining minutes in the oven based on the actual minutes provided as an argument.

- **Instance Method (`def preparation_time_in_minutes`):** Another method calculates the preparation time in minutes based on the number of layers.

- **Instance Method (`def total_time_in_minutes`):** This method calculates the total cooking time in minutes by combining the actual minutes in the oven and the preparation time, taking the number of layers into account.

**Usage:**

To use these methods, one can create an instance of the `Lasagna` class and call the methods on it:

```ruby
lasagna = Lasagna.new
puts lasagna.remaining_minutes_in_oven(30)
puts lasagna.preparation_time_in_minutes(3)
puts lasagna.total_time_in_minutes(number_of_layers: 3, actual_minutes_in_oven: 30)
```

This code would output the calculated values based on the provided methods.

In Java, a similar class and methods could be created, and the usage would be conceptually analogous, though with Java's syntax.

```java
public class Lasagna {

    private static final int EXPECTED_MINUTES_IN_OVEN = 40;

    public int remainingMinutesInOven(int actualMinutesInOven) {
        return EXPECTED_MINUTES_IN_OVEN - actualMinutesInOven;
    }

    public int preparationTimeInMinutes(int layers) {
        return layers * 2;
    }

    public int totalTimeInMinutes(int numberOfLayers, int actualMinutesInOven) {
        return actualMinutesInOven + preparationTimeInMinutes(numberOfLayers);
    }

    public static void main(String[] args) {
        Lasagna lasagna = new Lasagna();
        System.out.println(lasagna.remainingMinutesInOven(30));
        System.out.println(lasagna.preparationTimeInMinutes(3));
        System.out.println(lasagna.totalTimeInMinutes(3, 30));
    }
}
```

## Amusement Park Exercise

```ruby
class Attendee
  attr_accessor :height
  attr_accessor :pass_id
  
  def initialize(height)
    @height = height
  end

  def issue_pass!(pass_id)
    @pass_id = pass_id
  end

  def revoke_pass!
    @pass_id = nil
  end
end
```

**Ruby Explanation:**

- **Class Declaration (`class Attendee`):** The `class Attendee` syntax is used to define a class named `Attendee`. In Ruby, classes encapsulate behavior.

- **Attributes (`attr_accessor :height` and `attr_accessor :pass_id`):** `attr_accessor` is a Ruby shortcut to create getter and setter methods for instance variables. It allows reading and writing the values of `height` and `pass_id`.

- **Constructor (`def initialize`):** The `initialize` method serves as the constructor, initializing the `height` attribute when an instance of the class is created.

- **Instance Method (`def issue_pass!`):** This method assigns a value to the `pass_id` attribute when called.

- **Instance Method (`def revoke_pass!`):** This method sets the `pass_id` attribute to `nil`, revoking the pass.

**Usage:**

To use these methods and attributes, one can create an instance of the `Attendee` class and interact with it:

```ruby
attendee = Attendee.new(150)
puts attendee.height
attendee.issue_pass!("123")
puts attendee.pass_id
attendee.revoke_pass!
puts attendee.pass_id
```

In Java, a similar class and methods could be created, and the usage would be conceptually analogous, though with Java's syntax.

```java
public class Attendee {

    private int height;
    private String passId;

    public Attendee(int height) {
        this.height = height;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getPassId() {
        return passId;
    }

    public void issuePass(String passId) {
        this.passId = passId;
    }

    public void revokePass() {
        this.passId = null;
    }

    public static void main(String[] args) {
        Attendee attendee = new Attendee(150);
        System.out.println(attendee.getHeight());
        attendee.issuePass("123");
        System.out.println(attendee.getPassId());
        attendee.revokePass();
        System.out.println(attendee.getPassId());
    }
}
```

## Amusement Park Improvements Exercise

```ruby
class Attendee
  attr_accessor :height
  attr_accessor :pass_id

  def initialize(height)
    @height = height
    @pass_id = nil
  end

  def issue_pass!(pass_id)
    self.pass_id = pass_id
  end

  def revoke_pass!
    self.pass_id = nil
  end

  def has_pass?
    pass_id != nil
  end

  def fits_ride?(ride_minimum_height)
    height >= ride_minimum_height
  end

  def allowed_to_ride?(ride_minimum_height)
    return false unless has_pass?
    fits_ride?(ride_minimum_height)
  end
end
```

**Ruby Explanation:**

In this improved version of the `Attendee` class, additional methods are introduced to enhance functionality.

#### New Methods:

- **Instance Method (`def has_pass?`):** Checks if the attendee has a pass by evaluating whether `pass_id` is not `nil`.

- **Instance Method (`def fits_ride?`):** Determines if the attendee's height meets the minimum height requirement for a ride.

- **Instance Method (`def allowed_to_ride?`):** Checks if the attendee is allowed to ride, considering both having a pass and meeting the height requirement.

#### Usage:

```ruby
attendee = Attendee.new(160)
attendee.issue_pass!("789")

puts attendee.has_pass?
puts attendee.fits_ride?(150)
puts attendee.allowed_to_ride?(170)
```
Certainly, let's dive deeper into the Ruby specifics of the "Amusement Park Improvements" exercise. This exercise introduces a few concepts that haven't been explicitly covered in the previous explanations.

#### 1. `attr_accessor`:

In the `Attendee` class, you'll notice the use of `attr_accessor` for `height` and `pass_id`. This is a Ruby shortcut to create getter and setter methods for instance variables.

```ruby
attr_accessor :height
attr_accessor :pass_id
```

The `attr_accessor` creates both a getter (e.g., `height`) and a setter (e.g., `height=`) method for the specified instance variable. This simplifies the code and adheres to Ruby's principle of providing easy access to object attributes.

#### 2. `initialize` Method:

The `initialize` method is a special method in Ruby classes. It gets called when an object is created using `.new`. In this case, it sets the initial values for `height` and `pass_id`.

```ruby
def initialize(height)
  @height = height
  @pass_id = nil
end
```

#### 3. `self.pass_id` vs `pass_id`:

In Ruby, using `self.pass_id` inside a method refers to the instance variable `@pass_id`. This is similar to using `this.passId` in Java. When assigning or accessing instance variables within a class, using `self` is optional.

#### 4. Interrogative Methods:

Ruby conventionally uses a question mark (`?`) at the end of method names that return boolean values. In the `Attendee` class:

```ruby
def has_pass?
  pass_id != nil
end
```

Here, `has_pass?` is an interrogative method returning `true` or `false` based on whether the attendee has a pass.

These additional methods provide more capabilities for checking the attendee's eligibility to ride and whether they possess a pass.

```java
public class Attendee {
    private int height;
    private String passId;

    public Attendee(int height) {
        this.height = height;
        this.passId = null;
    }

    public void issuePass(String passId) {
        this.passId = passId;
    }

    public void revokePass() {
        this.passId = null;
    }

    public boolean hasPass() {
        return passId != null;
    }

    public boolean fitsRide(int rideMinimumHeight) {
        return height >= rideMinimumHeight;
    }

    public boolean allowedToRide(int rideMinimumHeight) {
        if (!hasPass()) {
            return false;
        }
        return fitsRide(rideMinimumHeight);
    }
}
```

The Java equivalent of the `Attendee` class mirrors the Ruby implementation, introducing getter and setter methods for the `height` and `passId` attributes. The logic and structure are adapted to Java's syntax and conventions.

#### New Methods:

- **Public Method (`public boolean hasPass()`):** Checks if the attendee has a pass by evaluating whether `passId` is not `null`.

- **Public Method (`public boolean fitsRide(int rideMinimumHeight)`):** Determines if the attendee's height meets the minimum height requirement for a ride.

- **Public Method (`public boolean allowedToRide(int rideMinimumHeight)`):** Checks if the attendee is allowed to ride, considering both having a pass and meeting the height requirement.

#### Usage:

```java
Attendee attendee = new Attendee(160);
attendee.issuePass("789");

System.out.println(attendee.hasPass());
System.out.println(attendee.fitsRide(150));
System.out.println(attendee.allowedToRide(170));
```

## Log Line Parser Exercise

```ruby
class LogLineParser
  def initialize(line)
    @log_level, @message = line.split(':')
  end

  def message
    @message.strip
  end

  def log_level
    @log_level.downcase.gsub(/[^a-z]/, "")
  end

  def reformat
    "#{message} (#{log_level})"
  end
end
```

- **Class Declaration (`class LogLineParser`):** The `LogLineParser` class is declared to encapsulate the logic for parsing log lines.

- **Initializer Method (`initialize(line)`):** The `initialize` method is a special method called when a new instance is created. It splits the input line into `@log_level` and `@message`.

- **Instance Method (`message`):** The `message` method returns the stripped message, removing leading and trailing whitespaces.

- **Instance Method (`log_level`):** The `log_level` method converts the log level to lowercase and removes non-alphabetic characters, providing a clean representation.

- **Instance Method (`reformat`):** The `reformat` method utilizes string interpolation to create a new formatted string combining the cleaned message and log level.

**String Manipulation:**

#### Stripping Whitespace

```ruby
text = "   Trim me!   "
trimmed_text = text.strip
```

The `strip` method removes leading and trailing whitespaces from a string. In the Log Line Parser exercise, this is employed in the `message` method to ensure a clean message.

#### Case Conversion

```ruby
text = "Info"
downcased_text = text.downcase
```

The `downcase` method converts a string to lowercase. In the Log Line Parser exercise, this is used in the `log_level` method to standardize log levels.

#### Substitution

```ruby
sentence = "I love Ruby programming."
new_sentence = sentence.gsub("Ruby", "Python")
```

The `gsub` method globally substitutes all occurrences of a substring in a string. While not explicitly used in the Log Line Parser exercise, it showcases another powerful string manipulation technique.

These string manipulation techniques are essential in the Log Line Parser exercise for processing and formatting log lines effectively. They enhance your ability to work with textual data in Ruby, making your code more robust and readable.

**Usage:**

To use this class, create an instance and call the methods:

```ruby
log_line = "ERROR: Something went wrong"
parser = LogLineParser.new(log_line)

puts parser.message
puts parser.log_level
puts parser.reformat
```

**Ruby-Specific Concepts:**

- **Instance Variables (`@log_level` and `@message`):** These are prefixed with `@` and are used to store values associated with an instance of the class.

- **Regular Expression (`/[^a-z]/):** This regular expression is used with `gsub` to remove non-alphabetic characters from the log level.

The logic and structure are adapted to Java's syntax and conventions:

```java
public class LogLineParser {

    private String logLevel;
    private String message;

    public LogLineParser(String line) {
        String[] parts = line.split(":");
        this.logLevel = parts[0];
        this.message = parts[1];
    }

    public String getMessage() {
        return message.trim();
    }

    public String getLogLevel() {
        return logLevel.toLowerCase().replaceAll("[^a-z]", "");
    }

    public String reformat() {
        return getMessage() + " (" + getLogLevel() + ")";
    }
}
```

## Assembly Line Exercise

```ruby
class AssemblyLine
  attr_reader :speed

  def initialize(speed)
    @speed = speed
  end

  def production_rate_per_hour
    rate = case speed
           when 1..4
             1.0
           when 5..8
             0.9
           when 9
             0.8
           when 10
             0.77
           else
             0.0
           end
    speed * 221 * rate
  end

  def working_items_per_minute
    production_rate_per_hour.div(60)
  end
end
```

1. **Case Statement (`case speed ... end`):** Ruby's `case` statement is a powerful tool for comparing a value against multiple conditions. In this case, the `speed` variable is checked against different ranges, and the corresponding rate is assigned.

    ```ruby
    rate = case speed
           when 1..4
             1.0
           when 5..8
             0.9
           when 9
             0.8
           when 10
             0.77
           else
             0.0
           end
    ```

    - **Ranges (`1..4`, `5..8`, ...):** Ruby allows the use of ranges to check if a value falls within a specific range.

    - **`else` Clause:** If none of the specified ranges match the value of `speed`, the `else` clause is executed, assigning a default value of `0.0` to `rate`.

2. **Method Chaining (`production_rate_per_hour.div(60)`):** Ruby allows methods to be chained together, creating a more concise and readable code. In this case, the result of `production_rate_per_hour` is divided by 60 using the `div` method.

    ```ruby
    def working_items_per_minute
      production_rate_per_hour.div(60)
    end
    ```

3. **Symbol (`:speed`):** While not explicitly used in this exercise, Ruby has a special type called symbols denoted by a colon (`:`). Symbols are lightweight identifiers often used as keys in hashes and for other purposes.

**Usage:**

To use this class, create an instance and call the methods:

```ruby
assembly_line = AssemblyLine.new(8)
puts assembly_line.production_rate_per_hour
puts assembly_line.working_items_per_minute
```

### Java Comparison:

```java
public class AssemblyLine {

    private final int speed;

    public AssemblyLine(int speed) {
        this.speed = speed;
    }

    public double productionRatePerHour() {
        double rate;
        switch (speed) {
            case 1, 2, 3, 4:
                rate = 1.0;
                break;
            case 5, 6, 7, 8:
                rate = 0.9;
                break;
            case 9:
                rate = 0.8;
                break;
            case 10:
                rate = 0.77;
                break;
            default:
                rate = 0.0;
                break;
        }
        return speed * 221 * rate;
    }

    public double workingItemsPerMinute() {
        return productionRatePerHour() / 60;
    }

    public static void main(String[] args) {
        AssemblyLine assemblyLine = new AssemblyLine(8);
        System.out.println(assemblyLine.productionRatePerHour());
        System.out.println(assemblyLine.workingItemsPerMinute());
    }
}
```

1. **Switch Statement (`switch`):** Java's `switch` statement is employed for comparing the value of `speed` against different cases, similar to Ruby's `case` statement.

    ```java
    switch (speed) {
        case 1, 2, 3, 4:
            rate = 1.0;
            break;
        case 5, 6, 7, 8:
            rate = 0.9;
            break;
        // ... other cases ...
        default:
            rate = 0.0;
            break;
    }
    ```

    - **Multiple Values in Case (`case 1, 2, 3, 4`):** Java allows combining multiple values in a single case label, providing a concise way to handle multiple cases.

    - **Default Case (`default`):** If none of the specified cases match the value of `speed`, the `default` case is executed, assigning a default value of `0.0` to `rate`.

2. **Method Naming Conventions (`productionRatePerHour`, `workingItemsPerMinute`):** Java follows camelCase naming conventions for method names, where each word (except the first) is capitalized.

3. **Final Keyword (`final`):** In Java, the `final` keyword is used to create constants or indicate that a variable cannot be reassigned. In this case, `speed` is declared as `final` to make it immutable once initialized.

```java
public class AssemblyLine {
    private final int speed;

    public AssemblyLine(int speed) {
        this.speed = speed;
    }
}
```

### Savings Account Exercise

```ruby
module SavingsAccount
  MIN_BALANCE = (...0)
  LOW_BALANCE = (0...1000)
  MEDIUM_BALANCE = (1000...5000)
  HIGH_BALANCE = (5000...)

  def self.interest_rate(balance)
    case balance
    when MIN_BALANCE
      3.213
    when LOW_BALANCE
      0.5
    when MEDIUM_BALANCE
      1.621
    when HIGH_BALANCE
      2.475
    end
  end

  def self.annual_balance_update(balance)
    balance + (interest_rate(balance) * balance / 100)
  end

  def self.years_before_desired_balance(current_balance, desired_balance)
    years = 0
    while current_balance < desired_balance
      current_balance = annual_balance_update(current_balance)
      years += 1
    end
    years
  end
end
```

- **Module Declaration (`module SavingsAccount`):** In Ruby, a module is a way to encapsulate methods, constants, and classes. Modules serve as namespaces, grouping related functionalities.

- **Constants (`MIN_BALANCE`, `LOW_BALANCE`, `MEDIUM_BALANCE`, `HIGH_BALANCE`):** Ruby allows the definition of constant ranges using the `(...)` and `...` notation. These constants represent different balance ranges.

- **Module Methods (`self.interest_rate`, `self.annual_balance_update`, `self.years_before_desired_balance`):** The `self` prefix indicates that these methods are part of the module itself (similar to static methods in Java). These methods calculate the interest rate, perform an annual balance update, and determine the number of years to reach a desired balance.

- **Case Statement (`case balance`):** Ruby's `case` statement checks the value of the `balance` variable against different constant ranges. Each range corresponds to a specific interest rate.

- **While Loop (`while current_balance < desired_balance`):** A `while` loop is used to iterate until the `current_balance` surpasses the `desired_balance`. Inside the loop, the `annual_balance_update` method is called to simulate the annual balance update.

### Ruby-Specific Concepts:

#### Constant Ranges:

```ruby
MIN_BALANCE = (...0)
LOW_BALANCE = (0...1000)
MEDIUM_BALANCE = (1000...5000)
HIGH_BALANCE = (5000...)
```

Ruby's constant ranges provide a concise way to define specific value ranges, enhancing code readability.
This Java equivalent replicates the functionality with adjustments based on Java's syntax and type system.

```java
public class SavingsAccount {

    private static final int MIN_BALANCE = 0;
    private static final int LOW_BALANCE = 1000;
    private static final int MEDIUM_BALANCE = 5000;

    public static double interestRate(int balance) {
        if (balance < MIN_BALANCE) {
            return 3.213;
        } else if (balance < LOW_BALANCE) {
            return 0.5;
        } else if (balance < MEDIUM_BALANCE) {
            return 1.621;
        } else {
            return 2.475;
        }
    }

    public static double annualBalanceUpdate(int balance) {
        return balance + (interestRate(balance) * balance / 100);
    }

    public static int yearsBeforeDesiredBalance(int currentBalance, int desiredBalance) {
        int years = 0;
        while (currentBalance < desiredBalance) {
            currentBalance = (int) annualBalanceUpdate(currentBalance);
            years++;
        }
        return years;
    }
}
```
